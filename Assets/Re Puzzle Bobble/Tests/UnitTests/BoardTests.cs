﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace RePuzzleBobble {
    public class BoardTests {
        [Test]
        public void AddBubble1At3By0_Empty_GetBubbleAt3By0Returns1() {
            Board board = new Board();
            board.AddBubbleAt( 1, new Hex( 3, 0 ) );
            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 3, 0 ) ) );
        }

        [Test]
        public void AddBubble0At3By0_Empty_ThrowsArgumentException() {
            Board board = new Board();
            Assert.Throws<ArgumentException>( () => board.AddBubbleAt( 0, new Hex( 3, 0 ) ) );
        }

        [Test]
        public void AddBubble1At3By0_AlreadyExistedBubbleAt3By0_ThrowsBoardException() {
            Board board = new Board();
            board.AddBubbleAt( 1, new Hex( 3, 0 ) );
            Assert.Throws<BoardException>( () => board.AddBubbleAt( 1, new Hex( 3, 0 ) ) );
        }

        [Test]
        public void GetBubbleAt3By0_NoBubbleAt3By0_Returns0() {
            Board board = new Board();
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 3, 0 ) ) );
        }

        [Test]
        public void RemoveBubbleFrom3By0_AlreadyExistedBubbleAt3By0_GetBubbleAt3By0Returns0() {
            Board board = new Board();
            board.AddBubbleAt( 1, new Hex( 3, 0 ) );
            board.RemoveBubbleFrom( new Hex( 3, 0 ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 3, 0 ) ) );
        }

        [Test]
        public void RemoveBubbleFrom3By0_NoBubbleAt3By0_ThrowsBoardException() {
            Board board = new Board();
            Assert.Throws<BoardException>( () => board.RemoveBubbleFrom( new Hex( 3, 0 ) ) );
        }

        [Test]
        public void ConstructWithLayout_Default_GetBubbleFromHexesReturnsCorrectBubbleIds() {
            int[][] layout = new int[][] {
                new int[] { 1, 2, 3, 2, 1, 3, 1, 2 },
                new int[] { 1, 3, 2, 1, 2, 0, 1 },
            };

            Board board = new Board( layout );

            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 0, 0 ) ) );
            Assert.AreEqual( 2, board.GetBubbleAt( new Hex( 1, 0 ) ) );
            Assert.AreEqual( 3, board.GetBubbleAt( new Hex( 2, 0 ) ) );
            Assert.AreEqual( 2, board.GetBubbleAt( new Hex( 3, 0 ) ) );
            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 4, 0 ) ) );
            Assert.AreEqual( 3, board.GetBubbleAt( new Hex( 5, 0 ) ) );
            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 6, 0 ) ) );
            Assert.AreEqual( 2, board.GetBubbleAt( new Hex( 7, 0 ) ) );

            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 0, 1 ) ) );
            Assert.AreEqual( 3, board.GetBubbleAt( new Hex( 1, 1 ) ) );
            Assert.AreEqual( 2, board.GetBubbleAt( new Hex( 2, 1 ) ) );
            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 3, 1 ) ) );
            Assert.AreEqual( 2, board.GetBubbleAt( new Hex( 4, 1 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 5, 1 ) ) );
            Assert.AreEqual( 1, board.GetBubbleAt( new Hex( 6, 1 ) ) );
        }

        [Test]
        public void CheckMatch3_TriangleMatch3_CountOfMatch3BubblesIs3() {
            int[][] layout = new int[][] {
                new int[] { 0, 0, 0, 1, 1, 0, 0, 0 },
                new int[] { 0, 0, 0, 1, 0, 0, 0 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfMatch3Bubbles = board.CheckMatch3( new Hex( 3, 1 ) );

            Assert.AreEqual( 3, hexesOfMatch3Bubbles.Count );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 3, 0 ) ) );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 4, 0 ) ) );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 3, 1 ) ) );
        }

        [Test]
        public void CheckMatch3_Match4_CountOfMatch3BubblesIs4() {
            int[][] layout = new int[][] {
                new int[] { 0, 0, 0, 1, 1, 1, 0, 0 },
                new int[] { 0, 0, 0, 1, 0, 0, 0 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfMatch3Bubbles = board.CheckMatch3( new Hex( 4, 0 ) );

            Assert.AreEqual( 4, hexesOfMatch3Bubbles.Count );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 3, 0 ) ) );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 4, 0 ) ) );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 5, 0 ) ) );
            Assert.AreEqual( true, hexesOfMatch3Bubbles.Contains( new Hex( 3, 1 ) ) );
        }

        [Test]
        public void GetFloatingBubbles_2FloatingBubbles_CountOfFloatingBubblesIs2() {
            int[][] layout = new int[][] {
                new int[] { 0, 0, 0, 0, 1, 0, 0, 0 },
                new int[] { 0, 2, 2, 0, 0, 0, 0 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfFloatingBubbles = board.GetFloatingBubbles();

            Assert.AreEqual( 2, hexesOfFloatingBubbles.Count );
            Assert.AreEqual( true, hexesOfFloatingBubbles.Contains( new Hex( 1, 1 ) ) );
            Assert.AreEqual( true, hexesOfFloatingBubbles.Contains( new Hex( 2, 1 ) ) );
        }

        [Test]
        public void GetFloatingBubbles_3FloatingBubbles_CountOfFloatingBubblesIs3() {
            int[][] layout = new int[][] {
                new int[] { 0, 0, 0, 0, 1, 1, 0, 0 },
                new int[] { 0, 2, 2, 0, 0, 0, 3 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfFloatingBubbles = board.GetFloatingBubbles();

            Assert.AreEqual( 3, hexesOfFloatingBubbles.Count );
            Assert.AreEqual( true, hexesOfFloatingBubbles.Contains( new Hex( 1, 1 ) ) );
            Assert.AreEqual( true, hexesOfFloatingBubbles.Contains( new Hex( 2, 1 ) ) );
            Assert.AreEqual( true, hexesOfFloatingBubbles.Contains( new Hex( 6, 1 ) ) );
        }

        [Test]
        public void InsertBubble1At0By2_WillMatch3_GetBubbleFromRemovedHexesRetures0AndCountOfRemovedBubblesIs3() {
            int[][] layout = new int[][] {
                new int[] { 1, 2, 3, 2, 1, 3, 1, 2 },
                new int[] { 1, 3, 2, 1, 2, 0, 1 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfRemovedBubbles = board.InsertBubble( 1, new Hex( 0, 2 ) );

            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 0, 0 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 0, 1 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 0, 2 ) ) );
            Assert.AreEqual( 3, hexesOfRemovedBubbles.Count );
        }

        [Test]
        public void InsertBubble3At3By4_WillMatch3AndExist1FloatingBubble_GetBubbleFromRemovedHexesRetures0AndGetBubbleFromFloatingHexesRetures0AndCountOfRemovedBubblesIs4() {
            int[][] layout = new int[][] {
                new int[] { 1, 2, 3, 2, 1, 3, 1, 2 },
                new int[] { 1, 3, 2, 1, 2, 0, 1 },
                new int[] { 2, 2, 1, 3, 0, 3, 0, 2 },
                new int[] { 0, 3, 0, 0, 2, 0, 0 },
            };

            Board board = new Board( layout );
            List<Hex> hexesOfRemovedBubbles = board.InsertBubble( 3, new Hex( 4, 2 ) );

            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 3, 2 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 4, 2 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 5, 2 ) ) );
            Assert.AreEqual( 0, board.GetBubbleAt( new Hex( 4, 3 ) ) );
            Assert.AreEqual( 4, hexesOfRemovedBubbles.Count );
        }
    }
}
