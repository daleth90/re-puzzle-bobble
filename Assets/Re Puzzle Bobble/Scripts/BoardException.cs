﻿using System;

namespace RePuzzleBobble {
    public class BoardException : Exception {
        public BoardException() : base() { }
        public BoardException( string message ) : base( message ) { }
        public BoardException( string message, Exception innerException ) : base( message, innerException ) { }
    }
}
