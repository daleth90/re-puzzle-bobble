﻿using UnityEngine;
using UnityEngine.UI;

namespace RePuzzleBobble {
    public class GameRoot : MonoBehaviour {
        [SerializeField]
        private Game game = null;
        [SerializeField]
        private GameObject gameClearPanel = null;
        [SerializeField]
        private GameObject gameOverPanel = null;
        [SerializeField]
        private Button retryButton = null;

        private void Start() {
            gameClearPanel.SetActive( false );
            gameOverPanel.SetActive( false );
            retryButton.gameObject.SetActive( false );

            game.GameCleared += OnGameCleared;
            game.GameOver += OnGameOver;
            retryButton.onClick.AddListener( ReloadStage );

            game.Initialize();
        }

        private void OnGameCleared() {
            gameClearPanel.SetActive( true );
            retryButton.gameObject.SetActive( true );
        }

        private void OnGameOver() {
            gameOverPanel.SetActive( true );
            retryButton.gameObject.SetActive( true );
        }

        private void ReloadStage() {
            gameClearPanel.SetActive( false );
            gameOverPanel.SetActive( false );
            retryButton.gameObject.SetActive( false );

            game.CleanUp();
            game.Initialize();
        }

        private void OnDestroy() {
            retryButton.onClick.RemoveListener( ReloadStage );
            game.GameOver -= OnGameOver;
        }
    }
}
