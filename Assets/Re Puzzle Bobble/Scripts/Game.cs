﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RePuzzleBobble {
    public class Game : MonoBehaviour {
        [SerializeField]
        private GameSetting gameSetting = null;
        [SerializeField]
        private Transform platform = null;
        [SerializeField]
        private Transform gameOverLine = null;
        [SerializeField]
        private Transform shooter = null;
        [SerializeField]
        private SpriteRenderer currentBubble = null;
        [SerializeField]
        private SpriteRenderer nextBubble = null;

        private enum State { FREE, BUBBLE_FLYING, GAME_OVER }

        public event Action GameCleared;
        public event Action GameOver;

        private static readonly int POWER_BUBBLE_ID = 100;

        private readonly Dictionary<Hex, Transform> bubbles = new Dictionary<Hex, Transform>();

        private Board board;
        private State state = State.FREE;

        private int numberOfShot;
        private float currentAngleOfShooter;
        private int currentBubbleId;
        private int nextBubbleId;

        private Transform flyingBubble;
        private int flyingBubbleId;
        private float flyingAngle;
        private bool isFlyingFinish;

        public void Initialize() {
            SetHeightOfPlatform( gameSetting.cabinetSetting.initialHeightOfPlatform );
            SetHeightOfGameOverLine( gameSetting.cabinetSetting.gameOverHeight );
            RandomBubbleOnShooter();

            int[][] randomStageLayout = RandomStageLayout();
            SetUpStage( randomStageLayout );
        }

        private int[][] RandomStageLayout() {
            int[][] stageLayout = new int[][] {
                new int[ gameSetting.cabinetSetting.fullNumberOfBubblePerRow ],
                new int[ gameSetting.cabinetSetting.fullNumberOfBubblePerRow - 1 ],
            };

            for ( int i = 0; i < gameSetting.cabinetSetting.fullNumberOfBubblePerRow; i++ ) {
                stageLayout[ 0 ][ i ] = RandomNormalBubbleId();
            }

            for ( int i = 0; i < gameSetting.cabinetSetting.fullNumberOfBubblePerRow - 1; i++ ) {
                stageLayout[ 1 ][ i ] = RandomNormalBubbleId();
            }

            return stageLayout;
        }

        private void SetUpStage( int[][] stageLayout ) {
            board = new Board( stageLayout );
            for ( int i = 0; i < stageLayout.Length; i++ ) {
                for ( int j = 0; j < stageLayout[ i ].Length; j++ ) {
                    int bubbleId = stageLayout[ i ][ j ];
                    if ( bubbleId != 0 ) {
                        SpriteRenderer newBubble = new GameObject( "Bubble" ).AddComponent<SpriteRenderer>();
                        newBubble.transform.SetParent( transform );
                        newBubble.transform.localPosition = HexToBubbleLocalPosition( new Hex( j, i ) );
                        newBubble.sprite = gameSetting.bubbleSetting.imagesOfBubbles[ bubbleId - 1 ];

                        bubbles.Add( new Hex( j, i ), newBubble.transform );
                    }
                }
            }
        }

        private void Update() {
            if ( state == State.FREE ) {
                // Rotate the shooter
                currentAngleOfShooter += -Input.GetAxis( "Horizontal" ) * gameSetting.cabinetSetting.rotateSpeedOfShooter * Time.deltaTime;
                if ( currentAngleOfShooter > gameSetting.cabinetSetting.maxAngleOfShooter ) {
                    currentAngleOfShooter = gameSetting.cabinetSetting.maxAngleOfShooter;
                }
                else if ( currentAngleOfShooter < -gameSetting.cabinetSetting.maxAngleOfShooter ) {
                    currentAngleOfShooter = -gameSetting.cabinetSetting.maxAngleOfShooter;
                }

                shooter.localEulerAngles = new Vector3( 0f, 0f, currentAngleOfShooter );

                // Shoot
                if ( Input.GetKeyDown( KeyCode.Space ) ) {
                    numberOfShot++;
                    StartShootProcess();
                    state = State.BUBBLE_FLYING;
                }
            }
            else if ( state == State.BUBBLE_FLYING ) {
                UpdateFlyingBubble();
                if ( isFlyingFinish ) {
                    Hex snapHex = GetBubbleSnapHex( flyingBubble.localPosition );
                    flyingBubble.localPosition = HexToBubbleLocalPosition( snapHex );
                    bubbles.Add( snapHex, flyingBubble );

                    if ( flyingBubbleId == POWER_BUBBLE_ID ) {
                        RemoveSelfAndAroundBubbles( snapHex );
                    }
                    else {
                        InsertBubble( flyingBubbleId, snapHex );
                    }

                    if ( numberOfShot % gameSetting.cabinetSetting.numberOfShotToDropPlatform == 0 ) {
                        platform.Translate( 0f, -gameSetting.cabinetSetting.dropHeightOfPlatform, 0f );
                        foreach ( Hex hex in bubbles.Keys ) {
                            bubbles[ hex ].localPosition = HexToBubbleLocalPosition( hex );
                        }
                    }

                    if ( bubbles.Count == 0 ) {
                        state = State.GAME_OVER;
                        GameCleared?.Invoke();
                        return;
                    }

                    float bottomLine = GetBottomLineOfBubbles();
                    if ( bottomLine - gameSetting.cabinetSetting.radiusOfBubbles < gameSetting.cabinetSetting.gameOverHeight ) {
                        state = State.GAME_OVER;
                        GameOver?.Invoke();
                        return;
                    }

                    state = State.FREE;
                }
            }
        }

        private void SetHeightOfPlatform( float height ) {
            Vector3 originalLocalPosition = platform.transform.localPosition;
            platform.transform.localPosition = new Vector3( originalLocalPosition.x, height, originalLocalPosition.z );
        }

        private void SetHeightOfGameOverLine( float height ) {
            Vector3 originalLocalPosition = gameOverLine.transform.localPosition;
            gameOverLine.transform.localPosition = new Vector3( originalLocalPosition.x, height, originalLocalPosition.z );
        }

        private void RandomBubbleOnShooter() {
            if ( nextBubbleId == 0 ) {
                nextBubbleId = RandomBubbleId();
            }

            currentBubbleId = nextBubbleId;
            nextBubbleId = RandomBubbleId();

            currentBubble.sprite = GetBubbleSprite( currentBubbleId );
            nextBubble.sprite = GetBubbleSprite( nextBubbleId );
        }

        private int RandomBubbleId() {
            if ( UnityEngine.Random.value < gameSetting.bubbleSetting.chanceOfPowerBubble ) {
                return POWER_BUBBLE_ID;
            }
            else {
                return RandomNormalBubbleId();
            }
        }

        private int RandomNormalBubbleId() {
            return UnityEngine.Random.Range( 1, gameSetting.bubbleSetting.imagesOfBubbles.Length + 1 );
        }

        private Sprite GetBubbleSprite( int bubbleId ) {
            if ( bubbleId == POWER_BUBBLE_ID ) {
                return gameSetting.bubbleSetting.imageOfPowerBubble;
            }
            else {
                return gameSetting.bubbleSetting.imagesOfBubbles[ bubbleId - 1 ];
            }
        }

        private void StartShootProcess() {
            ShootBubble();
            RandomBubbleOnShooter();
        }

        private void ShootBubble() {
            SpriteRenderer newBubble = new GameObject( "Bubble" ).AddComponent<SpriteRenderer>();
            newBubble.transform.SetParent( transform );
            newBubble.transform.localPosition = currentBubble.transform.localPosition;
            newBubble.sprite = currentBubble.sprite;

            flyingBubble = newBubble.transform;
            flyingBubbleId = currentBubbleId;
            flyingAngle = currentAngleOfShooter;
            isFlyingFinish = false;
        }

        private void UpdateFlyingBubble() {
            // Calculate the new position
            float radians = Mathf.Deg2Rad * ( 90f + flyingAngle );
            Vector3 direction = new Vector3( Mathf.Cos( radians ), Mathf.Sin( radians ), 0f );
            Vector3 translation = direction * gameSetting.cabinetSetting.speedOfFlyingBubble * Time.deltaTime;
            Vector3 newLocalPosition = flyingBubble.localPosition + translation;

            // Detect the walls and modify the position
            if ( flyingAngle < 0f && newLocalPosition.x + gameSetting.cabinetSetting.radiusOfBubbles > gameSetting.cabinetSetting.widthOfCabinet / 2f ) {
                flyingAngle = -flyingAngle;
                newLocalPosition.x = gameSetting.cabinetSetting.widthOfCabinet / 2f - gameSetting.cabinetSetting.radiusOfBubbles;
            }

            if ( flyingAngle > 0f && newLocalPosition.x - gameSetting.cabinetSetting.radiusOfBubbles < -gameSetting.cabinetSetting.widthOfCabinet / 2f ) {
                flyingAngle = -flyingAngle;
                newLocalPosition.x = -gameSetting.cabinetSetting.widthOfCabinet / 2f + gameSetting.cabinetSetting.radiusOfBubbles;
            }

            // Detect the platform and modify the position
            if ( newLocalPosition.y + gameSetting.cabinetSetting.radiusOfBubbles > platform.transform.localPosition.y ) {
                newLocalPosition.y = platform.transform.localPosition.y - gameSetting.cabinetSetting.radiusOfBubbles;
                isFlyingFinish = true;
            }

            // Detect other bubbles
            foreach ( Transform bubble in bubbles.Values ) {
                float distance = Vector3.Distance( bubble.localPosition, newLocalPosition );
                if ( distance < gameSetting.cabinetSetting.radiusOfBubbles * 2f ) {
                    isFlyingFinish = true;
                    break;
                }
            }

            // Set the position
            flyingBubble.localPosition = newLocalPosition;
        }

        private Hex GetBubbleSnapHex( Vector3 localPosition ) {
            int hexRow;
            float y = platform.transform.localPosition.y - localPosition.y;
            if ( y < gameSetting.cabinetSetting.radiusOfBubbles * ( 1f + Mathf.Sqrt( 3f ) / 2f ) ) {
                hexRow = 0;
            }
            else {
                float remain = y - gameSetting.cabinetSetting.radiusOfBubbles * ( 1f + Mathf.Sqrt( 3f ) / 2f );
                hexRow = (int)( remain / ( gameSetting.cabinetSetting.radiusOfBubbles * Mathf.Sqrt( 3f ) ) ) + 1;
            }

            int parity = hexRow & 1;
            int numberOfBubbleInThisRow;
            if ( parity == 0 ) {
                numberOfBubbleInThisRow = gameSetting.cabinetSetting.fullNumberOfBubblePerRow;
            }
            else {
                numberOfBubbleInThisRow = gameSetting.cabinetSetting.fullNumberOfBubblePerRow - 1;
            }

            float mostLeftX = -gameSetting.cabinetSetting.radiusOfBubbles * numberOfBubbleInThisRow;

            int hexColumn;
            if ( localPosition.x < mostLeftX ) {
                hexColumn = 0;
            }
            else if ( localPosition.x > -mostLeftX ) {
                hexColumn = numberOfBubbleInThisRow - 1;
            }
            else {
                hexColumn = (int)( ( localPosition.x - mostLeftX ) / ( gameSetting.cabinetSetting.radiusOfBubbles * 2f ) );
            }

            return new Hex( hexColumn, hexRow );
        }

        private Vector3 HexToBubbleLocalPosition( Hex hex ) {
            int parity = hex.row & 1;
            float mostLeftX;
            if ( parity == 0 ) {
                mostLeftX = -gameSetting.cabinetSetting.radiusOfBubbles * gameSetting.cabinetSetting.fullNumberOfBubblePerRow;
            }
            else {
                mostLeftX = -gameSetting.cabinetSetting.radiusOfBubbles * ( gameSetting.cabinetSetting.fullNumberOfBubblePerRow - 1 );
            }

            float x = mostLeftX + gameSetting.cabinetSetting.radiusOfBubbles * ( 1f + hex.column * 2f );
            float y = platform.localPosition.y - gameSetting.cabinetSetting.radiusOfBubbles * ( 1f + hex.row * Mathf.Sqrt( 3f ) );

            return new Vector3( x, y, 0f );
        }

        private void RemoveSelfAndAroundBubbles( Hex hex ) {
            // Remove the bubble at the hex
            RemoveBubbleFrom( hex );

            // Remove around bubbles
            Array values = Enum.GetValues( typeof( Hex.Direction ) );
            for ( int i = 0; i < values.Length; i++ ) {
                Hex neighbor = hex.GetNeighbor( (Hex.Direction)values.GetValue( i ) );
                if ( board.GetBubbleAt( neighbor ) != 0 ) {
                    board.RemoveBubbleFrom( neighbor );
                    RemoveBubbleFrom( neighbor );
                }
            }

            // Remove the floating bubbles due to this effect
            List<Hex> hexesOfFloatingBubbles = board.GetFloatingBubbles();
            for ( int i = 0; i < hexesOfFloatingBubbles.Count; i++ ) {
                board.RemoveBubbleFrom( hexesOfFloatingBubbles[ i ] );
                RemoveBubbleFrom( hexesOfFloatingBubbles[ i ] );
            }
        }

        private void InsertBubble( int bubbleId, Hex hex ) {
            List<Hex> hexesOfRemovedBubbles = board.InsertBubble( bubbleId, hex );
            for ( int i = 0; i < hexesOfRemovedBubbles.Count; i++ ) {
                RemoveBubbleFrom( hexesOfRemovedBubbles[ i ] );
            }
        }

        private void RemoveBubbleFrom( Hex hex ) {
            Transform bubble = bubbles[ hex ];
            bubbles.Remove( hex );
            Destroy( bubble.gameObject );
        }

        private float GetBottomLineOfBubbles() {
            int maxRow = 0;
            foreach ( Hex hex in bubbles.Keys ) {
                if ( hex.row > maxRow ) {
                    maxRow = hex.row;
                }
            }

            float bottomLine = platform.localPosition.y - gameSetting.cabinetSetting.radiusOfBubbles * ( 1f + maxRow * Mathf.Sqrt( 3f ) );
            return bottomLine;
        }

        public void CleanUp() {
            foreach ( Transform bubble in bubbles.Values ) {
                Destroy( bubble.gameObject );
            }
            bubbles.Clear();

            board = null;
            state = State.FREE;

            numberOfShot = 0;
            currentAngleOfShooter = 0f;
            currentBubbleId = 0;
            nextBubbleId = 0;

            if ( flyingBubble != null ) {
                Destroy( flyingBubble.gameObject );
                flyingBubble = null;
            }

            flyingBubbleId = 0;
            flyingAngle = 0f;
            isFlyingFinish = false;
        }
    }
}
