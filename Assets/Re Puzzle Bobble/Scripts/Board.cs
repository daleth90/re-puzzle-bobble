﻿using System;
using System.Collections.Generic;

namespace RePuzzleBobble {
    public class Board {
        private static readonly int EMPTY_ID = 0;
        private static readonly int MIN_MATCH_COUNT = 3;

        private readonly Dictionary<Hex, int> bubbles = new Dictionary<Hex, int>();

        public Board() {

        }

        public Board( int[][] boardLayout ) {
            for ( int i = 0; i < boardLayout.Length; i++ ) {
                for ( int j = 0; j < boardLayout[ i ].Length; j++ ) {
                    int bubbleId = boardLayout[ i ][ j ];
                    if ( bubbleId != EMPTY_ID ) {
                        AddBubbleAt( bubbleId, new Hex( j, i ) );
                    }
                }
            }
        }

        public void AddBubbleAt( int bubbleId, Hex hex ) {
            if ( bubbleId == EMPTY_ID ) {
                throw new ArgumentException( "Invalid to add bubbleId as 0" );
            }

            if ( bubbles.ContainsKey( hex ) ) {
                throw new BoardException( string.Format( "Try to add bubble {0} at {1} but the hex is not empty", bubbleId, hex.ToString() ) );
            }

            bubbles.Add( hex, bubbleId );
        }

        public void RemoveBubbleFrom( Hex hex ) {
            bool containsKey = bubbles.Remove( hex );
            if ( !containsKey ) {
                throw new BoardException( string.Format( "Try to remove bubble from {0} but the hex is empty", hex.ToString() ) );
            }
        }

        public int GetBubbleAt( Hex hex ) {
            if ( !bubbles.ContainsKey( hex ) ) {
                return EMPTY_ID;
            }

            return bubbles[ hex ];
        }

        public List<Hex> InsertBubble( int bubbleId, Hex hex ) {
            // Add the bubble to the board
            AddBubbleAt( bubbleId, hex );

            // Get and remove match-3 bubbles
            List<Hex> bubblesToBeRemovedFrom = CheckMatch3( hex );
            for ( int i = 0; i < bubblesToBeRemovedFrom.Count; i++ ) {
                RemoveBubbleFrom( bubblesToBeRemovedFrom[ i ] );
            }

            // Get and remove floating bubbles
            List<Hex> hexesOfFloatingBubbles = GetFloatingBubbles();
            for ( int i = 0; i < hexesOfFloatingBubbles.Count; i++ ) {
                RemoveBubbleFrom( hexesOfFloatingBubbles[ i ] );
            }

            // Collect and return the removed bubbles
            List<Hex> hexesOfRemovedBubbles = new List<Hex>();
            hexesOfRemovedBubbles.AddRange( bubblesToBeRemovedFrom );
            hexesOfRemovedBubbles.AddRange( hexesOfFloatingBubbles );
            return hexesOfRemovedBubbles;
        }

        public List<Hex> CheckMatch3( Hex startHex ) {
            List<Hex> closeHexes = new List<Hex>();
            List<Hex> openHexes = new List<Hex> { startHex };
            int rootId = GetBubbleAt( startHex );

            while ( openHexes.Count > 0 ) {
                Hex checkedHex = openHexes[ 0 ];
                openHexes.RemoveAt( 0 );

                int numberOfDirection = Enum.GetValues( typeof( Hex.Direction ) ).Length;
                for ( int i = 0; i < numberOfDirection; i++ ) {
                    Hex neighbor = checkedHex.GetNeighbor( (Hex.Direction)i );
                    if ( closeHexes.Contains( neighbor ) ) {
                        continue;
                    }

                    if ( openHexes.Contains( neighbor ) ) {
                        continue;
                    }

                    int bubbleId = GetBubbleAt( neighbor );
                    if ( bubbleId == rootId ) {
                        openHexes.Add( neighbor );
                    }
                }

                closeHexes.Add( checkedHex );
            }

            if ( closeHexes.Count < MIN_MATCH_COUNT ) {
                return new List<Hex>();
            }
            else {
                return closeHexes;
            }
        }

        public List<Hex> GetFloatingBubbles() {
            HashSet<Hex> checkedHexes = new HashSet<Hex>();
            List<Hex> hexesOfFloatingBubbles = new List<Hex>();

            foreach ( Hex hex in bubbles.Keys ) {
                if ( checkedHexes.Contains( hex ) ) {
                    continue;
                }

                bool isFloating = true;
                List<Hex> cluster = FindBubbleCluster( hex );
                for ( int i = 0; i < cluster.Count; i++ ) {
                    if ( cluster[ i ].row == 0 ) {
                        isFloating = false;
                        break;
                    }
                }

                for ( int i = 0; i < cluster.Count; i++ ) {
                    checkedHexes.Add( cluster[ i ] );
                }

                if ( isFloating ) {
                    hexesOfFloatingBubbles.AddRange( cluster );
                }
            }

            return hexesOfFloatingBubbles;
        }

        private List<Hex> FindBubbleCluster( Hex startHex ) {
            if ( GetBubbleAt( startHex ) == 0 ) {
                return new List<Hex>();
            }

            List<Hex> closeHexes = new List<Hex>();
            List<Hex> openHexes = new List<Hex> { startHex };

            while ( openHexes.Count > 0 ) {
                Hex checkedHex = openHexes[ 0 ];
                openHexes.RemoveAt( 0 );

                int numberOfDirection = Enum.GetValues( typeof( Hex.Direction ) ).Length;
                for ( int i = 0; i < numberOfDirection; i++ ) {
                    Hex neighbor = checkedHex.GetNeighbor( (Hex.Direction)i );
                    if ( closeHexes.Contains( neighbor ) ) {
                        continue;
                    }

                    int bubbleId = GetBubbleAt( neighbor );
                    if ( bubbleId != EMPTY_ID ) {
                        openHexes.Add( neighbor );
                    }
                }

                closeHexes.Add( checkedHex );
            }

            return closeHexes;
        }
    }
}
