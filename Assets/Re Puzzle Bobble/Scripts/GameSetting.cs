﻿using System;
using UnityEngine;

namespace RePuzzleBobble {
    [CreateAssetMenu( fileName = "GameSetting", menuName = "Game Setting" )]
    public class GameSetting : ScriptableObject {
        public CabinetSetting cabinetSetting;
        public BubbleSetting bubbleSetting;
    }

    [Serializable]
    public class CabinetSetting {
        public int fullNumberOfBubblePerRow = 7;
        public float initialHeightOfPlatform = 3f;
        public float gameOverHeight = -2.5f;
        public float dropHeightOfPlatform = 0.5f;
        public int numberOfShotToDropPlatform = 5;
        public float rotateSpeedOfShooter = 180f;
        public float maxAngleOfShooter = 60f;
        public float speedOfFlyingBubble = 5f;
        public float radiusOfBubbles = 0.32f;
        public float widthOfCabinet = 4.48f;
    }

    [Serializable]
    public class BubbleSetting {
        public float chanceOfPowerBubble = 0.05f;
        public Sprite imageOfPowerBubble;
        public Sprite[] imagesOfBubbles;
    }
}
