﻿namespace RePuzzleBobble {
    /// <summary>
    /// Use odd-r offset coordinates, see https://www.redblobgames.com/grids/hexagons/#coordinates-offset.
    /// </summary>
    public struct Hex {
        public enum Direction { RIGHT, LOWER_RIGHT, LOWER_LEFT, LEFT, UPPER_LEFT, UPPER_RIGHT }

        private static readonly Hex[][] directionOffsets = new Hex[ 2 ][] {
            new Hex[ 6 ] {new Hex( 1, 0 ), new Hex( 0, 1 ), new Hex( -1, 1 ), new Hex( -1, 0 ), new Hex( -1, -1 ), new Hex( 0, -1 ) },
            new Hex[ 6 ] {new Hex( 1, 0 ), new Hex( 1, 1 ), new Hex( 0, 1 ), new Hex( -1, 0 ), new Hex( 0, -1 ), new Hex( 1, -1 ) }
        };

        public int column;
        public int row;

        public Hex( int column, int row ) {
            this.column = column;
            this.row = row;
        }

        public override bool Equals( object obj ) {
            if ( !( obj is Hex ) ) {
                return false;
            }

            Hex other = (Hex)obj;
            return other.column == column && other.row == row;
        }

        public override int GetHashCode() {
            var hashCode = 815904954;
            hashCode = hashCode * -1521134295 + column.GetHashCode();
            hashCode = hashCode * -1521134295 + row.GetHashCode();
            return hashCode;
        }

        public override string ToString() {
            return string.Format( "( {0}, {1} )", column.ToString(), row.ToString() );
        }

        public Hex GetNeighbor( Direction direction ) {
            int parity = row & 1;
            Hex offset = directionOffsets[ parity ][ (int)direction ];
            return new Hex( column + offset.column, row + offset.row );
        }
    }
}
